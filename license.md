Copyright 2020 Neo_Fox / Lukáš Muller

This code is offered with no license. This means that all rights are reserved; no one may reproduce, distribute, or create derivative works from this work.

In plain English, what I *don't* want is for people to redistribute or monetize this work, but I *do* want people to be able to learn from it.

